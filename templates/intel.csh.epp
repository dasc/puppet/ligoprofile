<%- |
    String $iccversion,
| -%>
# This file is being maintained by Puppet.
# DO NOT EDIT
#
# LIGO-specific setup for just ICC or full Intel Parallel Studio
#
# Just icc added to PATH by default (user opt-out)
# opt-in for full Intel environemnt including Python and other tools (psxevars)
#
# ~/.nointel implies no icc or environment setup
# ~/.useintel_2019u2 setup specific environment (including icc)
# ~/.useicc_208u4 setup specific icc environment (not general intel environment)
# None of the above then just add default version of icc to PATH
# 
# Stuart Anderson (July 15, 2019)
#

if ( $USER == "root" ) then
  exit 0
endif

#
# Default ICC location and version
#
set intelroot=/ldcg/intel
set iccdefaultvers=<%= $iccversion %>



#
# only print messages when in a login shell, i.e., $0 is "-csh" or "-tcsh"
#
alias user_message 'if ( X$0:s/tcsh//:s/csh// == "X-" ) echo \!*'


#
# Glob for user opt-in and opt-out files in their home directory.
# No csh equivalent of shopt -s nullglob so check and remove the
# glob expression if present in the lists.
#
set nonomatch
set intellist = ($HOME/.useintel_*)
if ( "$intellist" == "$HOME/.useintel_*" ) then
  shift intellist
endif
set icclist = ($HOME/.useicc_*)
if ( "$icclist" == "$HOME/.useicc_*" ) then
  shift icclist
endif


#
# Process opt-in/opt-out requests
#
if ( -f $HOME/.nointel ) then
  #
  # Request no Intel setup whatsoever
  #
  user_message "INTEL: opt-out of setup requested."
else if ( $#intellist > 0 ) then
  #
  # Specific full Intel environment requested
  #
  if ( $#intellist > 1 ) then
    user_message "INTEL: Multiple versions requested: $intellist"
  endif

  foreach intelver ( $intellist )
    set intelvers=`echo $intelver | awk -F_ '{print $NF}' | tail -1`
    if ( -d $intelroot/$intelvers/bin ) then
      set intelfound=$intelvers
    endif
  end

  if ( $?intelfound ) then
      user_message "INTEL: Specified version $intelfound is active."
      source $intelroot/$intelfound/parallel_studio_xe_20??.*/psxevars.csh
      set license=needed
  else
      user_message "INTEL: Specified version(s) do not exist: $intellist"
      exit 0
  endif

  if ( $#icclist > 0 ) then
    #
    # Full Intel environemnt request takes precedence over specific ICC request
    #
    user_message "ICC version(s) ignored: $icclist"
  endif
else if ( $#icclist > 0 ) then
  #
  #  Specific ICC version requested (not full Intel environment)
  #
  if ( $#icclist > 1 ) then
    user_message "ICC: Multiple versions requested: $icclist"
  endif

  foreach iccver ( $icclist )
    set iccvers=`echo $iccver | awk -F_ '{print $NF}' | tail -1`
    if ( -d $intelroot/$iccvers/bin ) then
      set iccfound=$iccvers
    endif
  end

  if ( $?iccfound ) then
      user_message "ICC: Specified version $iccfound is active."
      setenv PATH ${PATH}:$intelroot/$iccfound/bin
      set license=needed
  else
      user_message "ICC: Specified version(s) do not exist: $icclist"
      exit 0
  endif
else
  #
  # No opt-in or opt-out requests so add default ICC version to PATH
  #
  if ( -d $intelroot/$iccdefaultvers/bin ) then
    user_message "ICC: Default version $iccdefaultvers is active."
    setenv PATH ${PATH}:$intelroot/$iccdefaultvers/bin
    set license=needed
  else
    user_message "ICC: Default version $iccdefaultvers not found."
  endif
endif 

# Intel compiler license location
if ( $?license ) then
  setenv INTEL_LICENSE_FILE $intelroot/licenses
  unset license
endif

unset intellist intelfound intelver intelvers icclist iccfound iccver iccvers iccdefaultvers
