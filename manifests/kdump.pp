class ligoprofile::kdump (
) {
    $localfilestore = lookup('ligoprofile::localfilestore')

    service {'kdump':
        ensure => 'running',
        enable => true,
    }

    if $facts['os']['family'] == 'RedHat' {
        case $facts['os']['release']['major'] {
            '7': {
                file {'/etc/kdump.conf':
                    ensure => 'file',
                    owner  => 'root',
                    group  => 'root',
                    mode   => '0644',
                    source => "${localfilestore}/etc/kdump.conf.el7",
                    notify => Service['kdump'],
                }
            }
            '8': {
                file {'/etc/kdump.conf':
                    ensure => 'file',
                    owner  => 'root',
                    group  => 'root',
                    mode   => '0644',
                    source => "${localfilestore}/etc/kdump.conf.el8",
                    notify => Service['kdump'],
                }
            }
        }
    }
}
