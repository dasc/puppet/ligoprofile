class ligoprofile::gpu (
) {
    $localfilestore = lookup('ligoprofile::localfilestore')

    $dedicated_gpus = lookup('ligoprofile::gpu::reserved', {default_value => {}})
    if $dedicated_gpus.length > 0 {
        file {'/etc/udev/rules.d/70-reserved-gpus.rules':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('ligoprofile/etc/udev/rules.d/70-reserved-gpus.rules.epp', {
                gpus => $dedicated_gpus,
            }),
            notify => Exec['udev reload nvidia'],
        }
    } else {
        file {'/etc/udev/rules.d/70-reserved-gpus.rules':
            ensure  => 'absent',
            notify => Exec['udev reload nvidia'],
        }
    }

    exec {'udev reload nvidia':
        command     => '/usr/sbin/udevadm trigger /sys/module/nvidia',
        refreshonly => true,
    }

    file { '/etc/profile.d/gpu.sh':
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        source => [ "${localfilestore}/etc/profile.d/gpu.sh",
        ],
    }
    file { '/etc/profile.d/gpu.csh':
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        source => [ "${localfilestore}/etc/profile.d/gpu.csh",
        ],
    }
}
