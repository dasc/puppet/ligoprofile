class ligoprofile::rsync::server (
    Boolean $use_xinetd,
) {
    include ligoprofile::rsync::base

    if lookup(rsync::server::modules, undef, undef, undef) {
        class {'rsync::server':
            use_xinetd     => $use_xinetd,
        }
    }
}
