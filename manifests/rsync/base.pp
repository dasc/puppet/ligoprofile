class ligoprofile::rsync::base (
    String $package_ensure,
)
{
    class {'rsync':
        package_ensure => $package_ensure,
    }
}
