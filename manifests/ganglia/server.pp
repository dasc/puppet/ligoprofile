class ligoprofile::ganglia::server (
) {
    class { '::ganglia':
			service_enable         => true,
			service_ensure         => 'running',
			cluster_name           => 'Servers',
			cluster_owner          => 'LIGO',
			cluster_latlong        => '34.136455,-118.123895',
			cluster_url            => 'http://www.ligo.caltech.edu',
			location               => 'Caltech',
			#allow_extra_data => 'allow_extra_data = yes',
			mute                   => false,
			deaf                   => true,
			cleanup_threshold      => 300,
			host_tmax              => 250,
			host_dmax              => 86400,
			send_metadata_interval => 60,
			tcp_accept_channel     => [{
				port => 8661,
			}],
			udp_send_channel       => [{
				host => '10.14.0.13',
				port => 8649,
				ttl  => 1,
			}],
		}
}
