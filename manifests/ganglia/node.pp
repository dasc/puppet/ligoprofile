class ligoprofile::ganglia::node (
) {
    class {'::ganglia': 
	service_enable => true,
	service_ensure => 'running',
        service_user => 'ganglia',
	cluster_name => 'Node',
	cluster_owner => 'LIGO',
	#allow_extra_data => 'allow_extra_data = yes',
	cluster_latlong => '',
	cluster_url => '',
        location => 'LIGO',
	mute => false,
	deaf => true,
	cleanup_threshold => 300,
	host_tmax => 250,
	host_dmax => 86400,
	send_metadata_interval => 60,
        tcp_accept_channel => [{
	    port => 8649,
	}],
	udp_send_channel => [{
	    host => '10.13.5.15',
	    port => 8649,
	    ttl => 1,
	}],
    }


}
