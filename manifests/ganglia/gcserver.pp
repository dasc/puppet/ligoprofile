class ligoprofile::ganglia::gcserver (
) {
    class {'::ganglia': 
	service_enable => true,
	service_ensure => 'running',
	cluster_name => 'LLO Server',
	cluster_owner => 'LIGO LLO',
	cluster_latlong => 'N42.3600 W71.0986',
	cluster_url => 'http://www.ligo-la.caltech.edu',
        location => 'LIGO LLO',
        #allow_extra_data => 'allow_extra_data = yes',
        override_hostname => true,
	mute => false,
	deaf => true,
	cleanup_threshold => 300,
	host_tmax => 250,
	host_dmax => 86400,
	send_metadata_interval => 60,
        tcp_accept_channel => [{
	    port => 8661,
	}],
	udp_send_channel => [{
	    host => '208.69.128.73',
	    port => 8661,
	    ttl => 1,
	}],
    }


}
