class ceph::gateway::nfs inherits ceph {
  include yum::repos::nfs::ganesha

  package { 'nfs-ganesha-ceph':
    ensure => present
  }

  file { '/etc/ganesha/ganesha.conf':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0644',
    source => 'puppet:///modules/ceph/etc/ganesha/ganesha.conf'
  }

  file { '/etc/ceph/client.ganesha.key':
    ensure => file,
    owner => 'ganesha',
    group => 'ganesha',
    mode => '0600',
    source => 'puppet:///modules/ceph/etc/ceph/client.ganesha.key'
  }
          
}
