class ceph {

  case $::os['family'] {

    'Debian': {
      include apt::repos::ceph
    }
 
    'RedHat': {
      include yum::repos::ceph
    }

  }

  file { '/etc/ceph':
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/etc/ceph/ceph.conf':
    require => File['/etc/ceph'],
    owner => 'root',
    group => 'root',
    mode => '0644',
    source => 'puppet:///modules/ceph/etc/ceph/ceph.conf'
  }

  file { '/usr/local/bin/crush-location':
    ensure => file,
    owner => root,
    group => root,
    mode => '0755',
    source => 'puppet:///modules/ceph/usr/local/bin/crush-location'
  }

}
