class ceph::client inherits ceph {

  package { 'ceph-common':
    ensure => present
  }

  file { '/ceph':
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/ceph/archive':
    require => File['/ceph'],
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/ceph/frames':
    require => File['/ceph'],
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/ceph/scratch':
    require => File['/ceph'],
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/etc/ceph/client.cephfs-archive.key':
    ensure => file,
    owner => 'ceph',
    group => 'ceph',
    mode => '0600',
    source => 'puppet:///modules/ceph/etc/ceph/client.cephfs-archive.key'
  }

  file { '/etc/ceph/client.cephfs-frames.key':
    ensure => absent
  }

  file { '/etc/ceph/client.cephfs-scratch.key':
    ensure => file,
    owner => 'ceph',
    group => 'ceph',
    mode => '0600',
    source => 'puppet:///modules/ceph/etc/ceph/client.cephfs-scratch.key'
  }

  file { '/opt/ceph':
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/opt/ceph/bin':
    require => File['/opt/ceph'],
    ensure => directory,
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/opt/ceph/bin/stage':
    require => File['/opt/ceph/bin'],
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0755',
    source => 'puppet:///modules/ceph/opt/ceph/bin/stage'
  }

  file { '/opt/ceph/bin/stager':
    ensure => absent
  }

}
