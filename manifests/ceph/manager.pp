class ceph::manager inherits ceph {

  package { [ 'ceph-mgr',
              'ceph-mgr-dashboard'] :
    ensure => present
  }

}  
