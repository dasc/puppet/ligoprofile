class ceph::mds inherits ceph {

  package { 'ceph-mds': 
    ensure => present
  }

}
