class ceph::osd inherits ceph {

  package { 'ceph-osd': 
    ensure => present
  }

  file { '/var/lib/ceph/bootstrap-osd/ceph.keyring':
    ensure => file,
    owner => 'ceph',
    group => 'ceph',
    mode => '0600',
    source => 'puppet:///modules/ceph/var/lib/ceph/bootstrap-osd/ceph.keyring'
  }

  file { '/etc/sysctl.d/70-ceph.conf':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0644',
    source => 'puppet:///modules/ceph/etc/sysctl.d/70-ceph.conf'
  }

  file { '/etc/rack':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0644',
    content => template('ceph/etc/rack.erb')
  }

  file { '/etc/chassis':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0644',
    content => template('ceph/etc/chassis.erb')
  }

  file { '/root/ceph-osd.deploy':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0755',
    content => template('ceph/root/ceph-osd.deploy.erb')
  }

  file { '/root/ceph-osd.purge':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0755',
    content => template('ceph/root/ceph-osd.purge.erb')
  }

  file { '/root/ceph-osd.replace':
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0755',
    content => template('ceph/root/ceph-osd.replace.erb')
  }

}
