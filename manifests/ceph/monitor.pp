class ceph::monitor inherits ceph {

  package { 'ceph-mon':
    ensure => present
  }

}
