class ligoprofile::ceph::base {
   package { ['ntp', 'ntpdate', 'ntp-doc']:
     ensure => present
   }
}