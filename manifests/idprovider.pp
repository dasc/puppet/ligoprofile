class ligoprofile::idprovider (
) {
    $localfilestore = lookup('ligoprofile::localfilestore')
    class {'::ldapclient':
        ldap_uri           => lookup('ligoprofile::ldap::uri'),
        ldap_base          => lookup('ligoprofile::ldap::base'),
        ldap_tlsreqcert    => lookup('ligoprofile::ldap::tlsreqcert'),
        ldap_tlscacert     => lookup('ligoprofile::ldap::tlscacert'),
        ldap_tlscacerttype => 'file',
        default_shell      => lookup('ligoprofile::ldap::defaultshell'),
    }

    $sssd_package_ensure = 'installed'
#    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] == '7' {
#        $sssd_package_ensure = '1.16.5-10.el7_9.15'
#    } elsif $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] == '8' {
#        $sssd_package_ensure = '2.7.3-4.el8_7.3'
#    } else {
#        $sssd_package_ensure = 'installed'
#    }
    class {'::sssd':
        sssd_package_ensure => $sssd_package_ensure,
        service_ensure      => 'running',
        mkhomedir           => false,
        extra_packages      => [],
        config_template     => 'ligoprofile/sssd/sssd.conf.erb',
        config              => {
            'sssd'           => {
                'domains'             => lookup('ligoprofile::sssd::domains'),
                'config_file_version' => 2,
                'services'            => ['nss', 'pam'],
            },
            'domain/default' => {
                'cache_credentials' => true,
                'ldap_search_base'      => lookup('ligoprofile::ldap::base'),
                'ldap_user_search_base' => lookup('ligoprofile::ldap::user::base'),
                'krb5_realm'            => lookup('ligoprofile::kerberos::realm'),
                'krb5_server'           => '',
                'id_provider'           => 'ldap',
                'auth_provider'         => 'krb5',
                'sudo_provider'         => 'none',
                'chpass_provider'       => 'krb5',
                'ldap_uri'              => lookup('ligoprofile::ldap::uri'),
                'ldap_tls_cacert'       => lookup('ligoprofile::ldap::tlscacert'),
                'ldap_tls_reqcert'      => lookup('ligoprofile::ldap::tlsreqcert'),
            },
            'domain/kagra'   => {
                'cache_credentials' => true,
                'ldap_search_base'  => lookup('ligoprofile::ldap::kagra'),
                'krb5_realm'        => lookup('ligoprofile::kerberos::realm'),
                'id_provider'       => 'ldap',
                'auth_provider'     => 'krb5',
                'sudo_provider'     => 'none',
                'chpass_provider'   => 'krb5',
                'ldap_uri'          => lookup('ligoprofile::ldap::uri'),
                'ldap_tls_cacert'   => lookup('ligoprofile::ldap::tlscacert'),
                'ldap_tls_reqcert'  => lookup('ligoprofile::ldap::tlsreqcert'),
            },
            'nss'            => {
                'default_shell' => lookup('ligoprofile::ldap::defaultshell'),
            },
        },
    }

    file {'/etc/openldap':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }
    file {'/etc/openldap/certs':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }

    file { '/etc/openldap/certs/ca-bundle.crt':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => [ "${localfilestore}/etc/openldap/certs/ca-bundle.crt",
        ],
    }

    file { '/etc/krb5.conf':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => [ "${localfilestore}/${facts['sitename']}/etc/krb5.conf.${facts['networking']['hostname']}",
            "${localfilestore}/${facts['sitename']}/etc/krb5.conf",
            "${localfilestore}/etc/krb5.conf",
        ],
    }

    $keytab_base64 = lookup('ligoprofile::kerberos::keytab', undef, undef, undef)
    if $keytab_base64 {
        file {'/etc/krb5.keytab':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0400',
            content => base64('decode', $keytab_base64),
        }
    }

    file {'/etc/bash_completion.d/id':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => [ "${localfilestore}/etc/bash_completion.d/id",
        ],
    }
}
