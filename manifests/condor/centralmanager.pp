class ligoprofile::condor::centralmanager (
    String $condor_version,
) {
    $condorfilestore = lookup('ligoprofile::condorfilestore')
    $configfilestore = "${condorfilestore}/etc/condor/config.d"
    $untrackedfilestore = lookup('ligoprofile::untrackedfilestore')

    # The condor-credmon-oauth package is not yet available for EL8
    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] < '8' {
        package {'condor-credmon-oauth':
            ensure          => 'absent',
            install_options => {
                '--enablerepo' => 'condor,condor-updates'
            },
        }
    }

    class {'::condor':
        package_ensure  => $condor_version,
        filestore       => $configfilestore,
        install_options => {
            '--enablerepo' => 'condor,condor-updates'
        },
    }

    $password_base64 = lookup('profile::condor::password_base64', undef, undef, undef)
    if $password_base64 {
        file {'/etc/condor/condor_cred':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0600',
            content => base64('decode', $password_base64),
            require => Package['condor'],
        }
    }

    condor::configfile { '00-base':
        source => "${condorfilestore}/etc/condor/config.d/00-base.cm",
    }
    condor::configfile { '00-logging': }
    condor::configfile { '00-security': ensure => absent, }
    condor::configfile { '00-htcondor-9.0.config': }
    condor::configfile { '10-jobmanagement': }
    condor::configfile { '10-quotas': }
    condor::configfile { '10-security':
        source => "${condorfilestore}/etc/condor/config.d/10-security.cm",
    }
    condor::configfile { '20-centralmanager': }
    condor::configfile { '20-misc': }
    condor::configfile { '40-oauth-credmon.conf': ensure => absent, }
    condor::configfile { '40-oauth-tokens.conf': }
}
