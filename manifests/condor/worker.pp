class ligoprofile::condor::worker (
    String $condor_version,
) {
    $condorfilestore = lookup('ligoprofile::condorfilestore')
    $configfilestore = "${condorfilestore}/etc/condor/config.d"

    exec {'condorworker-systemctl-daemon-reload':
        command     => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    class {'::condor':
        package_ensure  => $condor_version,
        filestore       => $configfilestore,
        install_options => {
            '--enablerepo' => 'condor,condor-updates'
        },
    }

    $password_base64 = lookup('profile::condor::password_base64', undef, undef, undef)
    if $password_base64 {
        file {'/etc/condor/condor_cred':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0600',
            content => base64('decode', $password_base64),
            require => Package['condor'],
        }
    }

        file {'/local':
            ensure => 'absent',
            owner  => 'root',
            group  => 'root',
            mode   => '1777',
        }

    file {'/etc/lvm/lvmlocal.conf':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => "${condorfilestore}/etc/lvm/lvmlocal.conf",
    }

    file {'/usr/libexec/condor/user_job_wrapper.sh':
        ensure  => 'file',
        owner   => 'condor',
        group   => 'condor',
        mode    => '0755',
        source  => "${condorfilestore}/usr/libexec/condor/user_job_wrapper.sh",
        require => Package['condor'],
    }

    file { '/etc/systemd/system/condor.service.d':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }

    file { '/etc/systemd/system/condor.service.d/memory.conf':
        require => File['/etc/systemd/system/condor.service.d'],
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('profile/etc/systemd/system/condor.service.d/memory.conf.epp'),
        notify  => Exec['condorworker-systemctl-daemon-reload'],
    }

    file { '/etc/systemd/system/condor.service.d/cgroup_delegation.conf':
        ensure  => 'absent',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => "[Service]\nDelegate=yes\n",
    }

    file { '/etc/condor/config.d/cpuinfo':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => "${condorfilestore}/etc/condor/config.d/cpuinfo",
    }

    file { '/etc/condor/modules/cpuinfo':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
        source => "${condorfilestore}/etc/condor/modules/cpuinfo",
    }
    file { '/etc/condor/config.d/facter': ensure => 'absent', }
    file { '/etc/condor/modules/facter': ensure => 'absent', }

    condor::configfile { '00-base': }
    condor::configfile { '00-logging': }
    condor::configfile { '00-htcondor-9.0.config': }
    condor::configfile { '00-security': ensure => absent, }
    condor::configfile { '10-jobmanagement': }
    condor::configfile { '10-quotas': }
    condor::configfile { '10-cgroups': }
    condor::configfile { '10-security': }
    if lookup('condor::configfile::20-batchnode', undef, undef, undef) {
        condor::configfile { '20-batchnode': }
    } else {
        condor::configfile { '20-batchnode':
            source => "${configfilestore}/20-batchnode.vg",
        }
    }
    condor::configfile { '20-schedd': ensure => absent, }
    condor::configfile { '20-misc': }
    condor::configfile { '30-gpu-nongpunode': }
    condor::configfile { '90-secure-cvmfs-ligo-osg': }
    condor::configfile { '90-singularity-osg': }
    condor::configfile { '99-request-disk': }
}
