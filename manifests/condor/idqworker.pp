class ligoprofile::condor::idqworker (
) {
    include ligoprofile::condor::worker

    $condorfilestore = lookup('ligoprofile::condorfilestore')

    file { '/etc/systemd/system/condor.service.d/20-idq.conf':
        require => File['/etc/systemd/system/condor.service.d'],
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => "${condorfilestore}/etc/systemd/system/condor.service.d/20-idq.conf",
        notify  => Exec['condorworker-systemctl-daemon-reload'],
    }
}
