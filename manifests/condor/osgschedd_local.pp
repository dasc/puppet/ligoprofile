class ligoprofile::condor::osgschedd_local (
    String $condor_version,
) {
    $condorfilestore = lookup('ligoprofile::condorfilestore')
    $configfilestore = "${condorfilestore}/etc/condor/config.d"
    $untrackedfilestore = lookup('ligoprofile::untrackedfilestore')

    # The condor-credmon-oauth package is not yet available for EL8
    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] < '8' {
        package {'condor-credmon-oauth':
            ensure          => 'absent',
            install_options => {
                '--enablerepo' => 'condor,condor-updates'
            },
        }
    }

    class {'::condor':
        package_ensure  => $condor_version,
        filestore       => $configfilestore,
        install_options => {
            '--enablerepo' => 'condor,condor-updates'
        },
    }
    package {'condor-credmon-vault':
        ensure => 'absent',
        install_options => {
            '--enablerepo' => 'condor,condor-updates'
        },
    }
    package {'condor-credmon-local':
        ensure => $condor_version,
        install_options => {
            '--enablerepo' => 'condor,condor-updates'
        },
    }

    $password_base64 = lookup('profile::condor::password_base64', undef, undef, undef)
    if $password_base64 {
        file {'/etc/condor/condor_cred':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0600',
            content => base64('decode', $password_base64),
            require => Package['condor'],
        }
    }

    file {'/local':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '1777',
    }

    file {['/local/condor', '/local/condor/spool', '/local/condor/checkpoint']:
        ensure  => 'directory',
        owner   => 'condor',
        group   => 'condor',
        mode    => '0755',
        seltype => 'condor_var_lib_t',
        require => Package['condor'],
    }

    file {'/local/condor/execute':
        ensure  => 'directory',
        owner   => 'condor',
        group   => 'condor',
        mode    => '1777',
        seltype => 'condor_var_lib_t',
        require => Package['condor'],
    }

    file { '/etc/cron.daily/local_cleanup':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
        content => "#!/bin/sh

find /local -path /local/condor -prune -o -path /local/BOINC -prune -o -type f -mtime +30 -print0  | xargs --null rm -f\n",
    }

    package {'condorview': ensure => absent, }

    file { '/etc/condor/accounting':
        ensure  => 'directory',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
        require => Package['condor'],
    }

    $user_file_content = file("/etc/puppetlabs/code/files/etc/condor/accounting/valid_users")
    if $user_file_content != '' {
        file { '/etc/condor/accounting/valid_users':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            source  => "${untrackedfilestore}/etc/condor/accounting/valid_users",
            notify  => Exec['condor_reconfig master'],
            require => Package['condor'],
        }
    } else {
        notify{'user file content': loglevel => 'warning', message => '/etc/puppetlabs/code/files/etc/condor/accounting/valid_users is EMPTY', }
    }

    $tag_file_content = file('/etc/puppetlabs/code/files/etc/condor/accounting/valid_tags')
    if $tag_file_content != '' {
        file { '/etc/condor/accounting/valid_tags':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            source  => "${untrackedfilestore}/etc/condor/accounting/valid_tags",
            notify  => Exec['condor_reconfig master'],
            require => Package['condor'],
        }
    } else {
        notify{'user file content': loglevel => 'warning', message => '/etc/puppetlabs/code/files/etc/condor/accounting/valid_tags is EMPTY', }
    }
    file { '/etc/cron.daily/condor_history':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
        content => epp('ligoprofile/condor_history_cron.epp'),
    }
    file { '/usr/bin/condor_validate_group_submit':
        ensure => 'absent',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
        source => [ "${condorfilestore}/usr/bin/condor_validate_group_submit.${hostname}",
            "${condorfilestore}/usr/bin/condor_validate_group_submit"
        ],
    }
    file { '/etc/condor/accounting_groups.json':
        ensure  => 'absent',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => [ "${untrackedfilestore}/etc/condor/accounting_groups.json.${hostname}",
            "${untrackedfilestore}/etc/condor/accounting_groups.json"
        ],
        require => Package['condor'],
    }
    file { '/etc/condor/accounting_groups_regex.json':
        ensure  => 'absent',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => [ "${condorfilestore}/etc/condor/accounting_groups_regex.json.${hostname}",
            "${condorfilestore}/etc/condor/accounting_groups_regex.json"
        ],
        require => Package['condor'],
    }
    file { '/etc/condor/accounting_shared_accounts.json':
        ensure  => 'absent',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => [ "${untrackedfilestore}/etc/condor/accounting_shared_accounts.json.${hostname}",
            "${untrackedfilestore}/etc/condor/accounting_shared_accounts.json"
        ],
        require => Package['condor'],
    }
    file { '/etc/condor/accounting_invalid.txt':
        ensure  => 'absent',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => [ "${condorfilestore}/etc/condor/accounting_invalid.txt.${hostname}",
            "${condorfilestore}/etc/condor/accounting_invalid.txt"
        ],
        require => Package['condor'],
    }
    file { '/etc/condor/accounting_none.txt':
        ensure  => 'absent',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => [ "${condorfilestore}/etc/condor/accounting_none.txt.${hostname}",
            "${condorfilestore}/etc/condor/accounting_none.txt"
        ],
        require => Package['condor'],
    }

    file { '/etc/condor/config.d/cpuinfo':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => "${condorfilestore}/etc/condor/config.d/cpuinfo",
        require => Package['condor'],
    }

    file { '/etc/condor/modules/cpuinfo':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
        source  => "${condorfilestore}/etc/condor/modules/cpuinfo",
        require => Package['condor'],
    }

    file { '/etc/condor/scitokens.d':
        ensure  => 'directory',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
        require => Package['condor'],
    }

    file { '/etc/condor/passwords.d':
        ensure  => 'directory',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        require => Package['condor'],
    }

    condor::configfile { '00-htcondor-9.0.config': }
    condor::configfile { '00-security': ensure => absent, }
    condor::configfile { '00-logging':
        source => "${condorfilestore}/etc/condor/config.d/00-logging.osgsubmit",
    }
    condor::configfile { '00_gwms_general.config': }
    condor::configfile { '02_gwms_schedds.config': }
    condor::configfile { '03_gwms_local.config': }
    condor::configfile { '10-accounting': }
    condor::configfile { '10-security':
        source => "${condorfilestore}/etc/condor/config.d/10-security.osgsubmit",
    }
    condor::configfile { '10-stash-plugin.conf': }
    condor::configfile { '40-oauth-credmon.conf':
      content   => epp('ligoprofile/etc/condor/config.d/40-oauth-credmon.conf.epp'),
    }
    condor::configfile { '40-vault-credmon.conf': ensure => absent,}
    condor::configfile { '60-osg-project-name': }
    condor::configfile { '90_flock_to_osg.config': }
    condor::configfile { '90_gwms_dns.config': }
    condor::configfile { '98-ha-central-manager.config': }
    condor::configfile { '99-addcredd.conf': }
    condor::configfile { '99_dagman_vars': }
    condor::configfile { '99_security': }
    condor::configfile { '99_test_flock': }
    condor::configfile { '99_test_network': }
    condor::configfile { '99_trust_domain': }
    condor::configfile { '99_zz_pelican_retry': }
}
