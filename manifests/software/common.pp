class ligoprofile::software::common (
) {
    $sitename = lookup('ligoprofile::sitename')
    $localfilestore = lookup('ligoprofile::localfilestore')

    exec {'software-common-systemctl-daemon-reload':
        command     => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    package {'osg-version': ensure => 'absent', }

    file {'/etc/sitename':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        #content => "${facts['sitename']}\n",
        content => "${sitename}\n",
    }

    if $facts['os']['name'] == 'Scientific' {
        file {'/etc/yum/vars/slreleasever':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => "${lookup('ligoprofile::os::majorversion')}.${lookup('ligoprofile::os::minorversion')}\n",
        }
    }

    case $facts['kernel'] {
        'SunOS': {
            package {'iftop': ensure => 'present', }
            package {'ocm': ensure => 'absent', }
            package {'watch': ensure => installed, }
        }
        default: {
            lookup('ligoprofile::software::common::packages', {default_value => {}} ).each | String $name, Optional[Hash] $packageinfo | {
                if $packageinfo {
                    package{$name:
                        * => $packageinfo,
                    }
                } else {
                    package{$name: ensure => 'installed', }
                }
            }
            service {'lldpd':
                ensure  => 'running',
                enable  => true,
                require => Package['lldpd'],
            }


            sysctl::settings {'80-magic-sysrq':
                settings => {
                    'kernel.sysrq' => '1',
                },
            }
        }
    }

    if $facts['dmi']['bios']['vendor'] == 'Dell Inc.' {
        package {'dell-system-update':
            ensure          => 'latest',
            install_options => {'--enablerepo' => 'dell-system-update_independent',},
        }
    }

    file {'/etc/updatedb.conf':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('ligoprofile/etc/updatedb.conf.epp', {
            'prunefs'    => lookup('ligoprofile::mlocate::prunefs'),
            'prunenames' => lookup('ligoprofile::mlocate::prunenames'),
            'prunepaths' => lookup('ligoprofile::mlocate::prunepaths'),
        })
    }

    if $facts['os']['family'] == 'RedHat' {
        package {'node_exporter': ensure => 'installed', }
        $node_exporter_args = lookup('ligoprofile::software::node_exporter::args')
        file {'/etc/sysconfig/node_exporter':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => "# Add extra command line options for the node_exporter here\nEXTRA_ARGS=${node_exporter_args}\n",
            require => Package['node_exporter'],
            notify  => Service['node_exporter'],
        }
        service {'node_exporter':
            ensure  => 'running',
            enable  => true,
            require => Package['node_exporter'],
        }

        sysctl::settings {'31-arp-table':
            settings => {
                'net.ipv4.neigh.default.gc_thresh1' => '2048',
                'net.ipv4.neigh.default.gc_thresh2' => '4096',
                'net.ipv4.neigh.default.gc_thresh3' => '8192',
            },
        }
        if $facts['os']['release']['major'] <= '8' {
            if ! $facts['is_virtual'] {
                file {'/etc/cron.d/smartd-exporter':
                    ensure  => 'file',
                    owner   => 'root',
                    group   => 'root',
                    mode    => '0644',
                    content => "*/5 * * * * root /var/scripts/smartmon.sh > /etc/node_exporter/txtfiles.d/smartd.prom\n",
                    require => Package['node_exporter'],
                }
            } else {
                file {'/etc/cron.d/smartd-exporter': ensure  => 'absent', }
                file {'/etc/node_exporter/txtfiles.d/smartd.prom': ensure  => 'absent', }
            }
            file {'/var/scripts/smartmon.sh':
                ensure  => 'file',
                owner   => 'root',
                group   => 'root',
                mode    => '0755',
                seluser => 'system_u',
                seltype => 'bin_t',
                source  => "${localfilestore}/var/scripts/smartmon.sh",
            }
        }
    }
}
