class ligoprofile::software::dmt (
) {
    lookup('ligoprofile::software::dmt::packages', {default_value => {}} ).each | String $name, Optional[Hash] $packageinfo | {
        if $packageinfo {
            package{"${name}":
                * => $packageinfo,
            }
        } else {
            package{"${name}": ensure => 'installed', }
        }
    }

    group {'gds':
        ensure => present,
        gid    => 1002,
    }

    user {'dmtexec':
        ensure  => present,
        uid     => 1004,
        groups  => 'gds',
        require => Group['gds'],
    }

    user {'calibration':
        ensure  => present,
        uid     => 1003,
        groups  => 'gds',
        require => Group['gds'],
    }

    file {'/var/spool/dmt-triggers':
        ensure => 'directory',
        owner  => 'dmtexec',
        group  => 'gds',
        mode   => '0755',
    }

    file {'/etc/security/limits.d/80-lowlatency.conf':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => "# This file is being maintained by Puppet.
# DO NOT EDIT
dmtexec - memlock 4200000
dmtexec - rtprio 99\n",
    }

    sysctl::settings {'01-ligo':
        settings => {
            'net.core.rmem_max' => '26214400',
            'net.core.rmem_default' => '26214400',
        },
    }

    file {'/var/log/journal':
        ensure => 'directory',
        owner  => 'root',
        group  => 'systemd-journal',
        mode   => '2755',
    }

    loginctl_user {'dmtexec':
        linger => enabled,
    }
}
