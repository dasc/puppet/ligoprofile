class ligoprofile::software::dotfiles (
) {
#    file { '/etc/profile.d/intel.sh':
#        ensure => 'file',
#        owner  => 'root',
#        group  => 'root',
#        mode   => '0644',
#        content => epp('ligoprofile/intel.sh.epp', {
#            iccversion => lookup('ligoprofile::iccversion'),
#        }),
#    }
#    file { '/etc/profile.d/intel.csh':
#        ensure => 'file',
#        owner  => 'root',
#        group  => 'root',
#        mode   => '0644',
#        content => epp('ligoprofile/intel.csh.epp', {
#            iccversion => lookup('ligoprofile::iccversion'),
#        }),
#    }

    file { '/etc/profile.d/intel.sh':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/ligoprofile/etc/profile.d/intel.sh',
    }
    file { '/etc/profile.d/intel.csh':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/ligoprofile/etc/profile.d/intel.csh',
    }

    file { '/etc/profile.d/jax.sh':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/ligoprofile/etc/profile.d/jax.sh',
    }
    file { '/etc/profile.d/jax.csh':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/ligoprofile/etc/profile.d/jax.csh',
    }
}
