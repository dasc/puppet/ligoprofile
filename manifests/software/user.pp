class ligoprofile::software::user (
) {
    $localfilestore = lookup('ligoprofile::localfilestore')

    lookup('ligoprofile::software::user::packages', {default_value => {}} ).each | String $name, Optional[Hash] $packageinfo | {
        if $packageinfo {
            package{"${name}":
                * => $packageinfo,
            }
        } else {
            package{"${name}": ensure => 'installed', }
        }
    }

    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] < '8' {
        package { 'NetworkManager-team': ensure => 'absent', } ->
        package { 'NetworkManager-tui': ensure => 'absent', } ->
        package { 'NetworkManager-ppp': ensure => 'absent', } ->
        package { 'NetworkManager': ensure => 'absent', } ->
        package { 'NetworkManager-libnm': ensure => 'absent', }
    }

    package { 'lalapps-debuginfo': ensure => 'absent', }
    package { 'lalinference-debuginfo': ensure => 'absent', }
    package { 'lalinspiral-debuginfo': ensure => 'absent', }
    package { 'lalpulsar-debuginfo': ensure => 'absent', }

    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] < '8' {
        # Prior version 11.0-3.el7
        package { 'devtoolset-11': ensure => '11.1-2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10': ensure => '10.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-binutils': ensure => '2.35-5.el7.3',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-dyninst': ensure => '10.2.1-1.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-elfutils': ensure => '0.182-3.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-elfutils-libelf': ensure => '0.182-3.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-elfutils-libs': ensure => '0.182-3.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-gcc': ensure => '10.2.1-11.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-gcc-c++': ensure =>  '10.2.1-11.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-gcc-gfortran': ensure =>  '10.2.1-11.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-gdb': ensure =>  '9.2-10.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-libquadmath-devel': ensure =>  '10.2.1-11.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-libstdc++-devel': ensure =>  '10.2.1-11.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-perftools': ensure =>  '10.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-runtime': ensure =>  '10.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-systemtap': ensure =>  '4.4-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-systemtap-client': ensure =>  '4.4-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-systemtap-devel': ensure =>  '4.4-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-systemtap-runtime': ensure =>  '4.4-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-10-toolchain': ensure =>  '10.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }

        package { 'devtoolset-9': ensure => '9.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-binutils': ensure => '2.32-16.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-elfutils': ensure => '0.176-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-elfutils-libelf': ensure => '0.176-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-elfutils-libs': ensure => '0.176-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-gcc': ensure => '9.3.1-2.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-gcc-c++': ensure => '9.3.1-2.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-gcc-gfortran': ensure => '9.3.1-2.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-libquadmath-devel': ensure => '9.3.1-2.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-libstdc++-devel': ensure => '9.3.1-2.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-ltrace': ensure => '0.7.91-2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-make': ensure => '4.2.1-2.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-perftools': ensure => '9.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-runtime': ensure => '9.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-strace': ensure => '5.1-7.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-systemtap': ensure => '4.1-9.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-systemtap-client': ensure => '4.1-9.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-systemtap-devel': ensure => '4.1-9.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-systemtap-runtime': ensure => '4.1-9.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'devtoolset-9-toolchain': ensure => '9.1-0.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }

        package { 'devtoolset-8': ensure => '8.1-1.el7',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'devtoolset-8-gcc': ensure => '8.3.1-3.2.0.1.el7',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'devtoolset-8-binutils': ensure => '2.30-55.0.1.el7.2',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'devtoolset-8-oprofile': ensure => 'installed',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'devtoolset-8-dyninst': ensure => 'installed',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'devtoolset-7': ensure => 'absent', } ->
        package { 'devtoolset-7-perftools': ensure => 'absent', } ->
        package { 'devtoolset-7-oprofile': ensure => 'absent', } ->
        package { 'devtoolset-7-dyninst': ensure => 'absent', } ->
        package { 'devtoolset-7-toolchain': ensure => 'absent', } ->
        package { 'devtoolset-7-gcc-gfortran': ensure => 'absent', } ->
        package { 'llvm-toolset-7': ensure => 'absent', } ->
        package { 'llvm-toolset-7-clang': ensure => 'absent', } ->
        package { 'llvm-toolset-7-lldb': ensure => 'absent', } ->
        package { 'llvm-toolset-7-clang-libs': ensure => 'absent', } ->
        package { 'llvm-toolset-7-llvm': ensure => 'absent', } ->
        package { 'llvm-toolset-7-llvm-libs': ensure => 'absent', } ->
        package { 'llvm-toolset-7-libomp': ensure => 'absent', } ->
        package { 'llvm-toolset-7-python2-lit': ensure => 'absent', } ->
        package { 'llvm-toolset-7-compiler-rt': ensure => 'absent', } ->
        package { 'llvm-toolset-7-runtime': ensure => 'absent', } ->
        package { 'devtoolset-7-gcc-c++': ensure => 'absent', } ->
        package { 'devtoolset-7-libquadmath-devel': ensure => 'absent', } ->
        package { 'devtoolset-7-gcc': ensure => 'absent', } ->
        package { 'devtoolset-7-strace': ensure => 'absent', } ->
        package { 'devtoolset-7-systemtap': ensure => 'absent', } ->
        package { 'devtoolset-7-binutils': ensure => 'absent', } ->
        package { 'devtoolset-7-make': ensure => 'absent', } ->
        package { 'devtoolset-7-gdb': ensure => 'absent', } ->
        package { 'devtoolset-7-libstdc++-devel': ensure => 'absent', } ->
        package { 'devtoolset-7-elfutils': ensure => 'absent', } ->
        package { 'devtoolset-7-dwz': ensure => 'absent', } ->
        package { 'devtoolset-7-memstomp': ensure => 'absent', } ->
        package { 'devtoolset-7-ltrace': ensure => 'absent', } ->
        package { 'devtoolset-7-valgrind': ensure => 'absent', } ->
        package { 'devtoolset-7-elfutils-libs': ensure => 'absent', } ->
        package { 'devtoolset-7-elfutils-libelf': ensure => 'absent', } ->
        package { 'devtoolset-7-systemtap-client': ensure => 'absent', } ->
        package { 'devtoolset-7-systemtap-runtime': ensure => 'absent', } ->
        package { 'devtoolset-7-systemtap-devel': ensure => 'absent', } ->
        package { 'devtoolset-7-runtime': ensure => 'absent', }

        package { 'devtoolset-6': ensure => absent, }
        package { 'devtoolset-4-runtime': ensure => absent, }
        package { 'devtoolset-4-dyninst': ensure => absent, }

        # Temporary place to put cluster-wide updates
        package { 'python27-scipy':
            ensure          => installed,
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'python27-python':
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'python27-python-pip':
            ensure          => '8.1.2-6.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'python27-python-virtualenv':
            ensure          => '13.1.0-4.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'python27':
            ensure          => 'installed',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'rh-python36-scipy':
            ensure          => installed,
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'rh-python36-python':
            ensure          => '3.6.12-1.0.1.el7',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
        package { 'rh-python36':
            ensure          => 'installed',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'rh-python36-python-pip':
            ensure          => '9.0.1-5.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'rh-python36-python-virtualenv':
            ensure          => '15.1.0-3.el7',
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'scl-utils-build':
            ensure          => installed,
            install_options => {'--enablerepo' => 'softwarecollections'},
        }
        package { 'libasan':
            ensure          => '4.9.2-6.2.el7',
            install_options => {'--enablerepo' => 'softwarecollections-oracle'},
        }
    }

    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] > '7' {
        file { '/etc/apptainer/apptainer.conf':
            ensure => 'file',
            owner => 'root',
            group => 'root',
            mode => '0644',
            require => Package['apptainer'],
            source => "${localfilestore}/etc/apptainer/apptainer.conf",
        }
        file { ['/etc/singularity/singularity.conf', '/etc/singularity/singularity.conf.rpmnew', '/etc/singularity/singularity.conf.rpmsave']:
            ensure => 'absent',
        }
        -> file { '/etc/singularity':
            ensure => 'absent',
        }
    }

    file {'/var/log/journal':
        ensure => 'directory',
        owner  => 'root',
        group  => 'systemd-journal',
        mode   => '2755',
    }
}
