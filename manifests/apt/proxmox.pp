class ligoprofile::apt::proxmox {

  file {'/etc/apt/sources.d/pve-enterprise.list':
    ensure => 'absent',
  }

  apt::key  { 'proxmox.com':
    server => 'hkps.pool.sks-keyservers.net',
    source => 'http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg',
    id     => '353479F83781D7F8ED5F5AC57BF2812E8A6E88E0',
  }

  apt::source { 'pve-no-subscription':
    location => 'http://download.proxmox.com/debian/pve',
    repos    => 'pve-no-subscription',
    release  => 'buster',
  }

  apt::source { 'ceph':
    location => 'http://download.proxmox.com/debian/ceph-nautilus',
    repos    => 'main',
    release  => 'buster',
  }
}
