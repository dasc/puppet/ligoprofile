class ligoprofile::apt::docker_ce {
  # remove packages :docker docker-engine docker.io containerd runc
  package { ['docker', 'docker-engine', 'docker.io','containerd', 'runc']:
    ensure => 'absent',
  }
  # add packages : apt-transport-https \
  #     ca-certificates \
  #     curl \
  #     gnupg2 \
  #     software-properties-common
  package { ['apt-transport-https', 'ca-certificates', 'curl','gnupg2', 'software-properties-common' ]:
    ensure => 'installed',
  }
  apt::key  { 'docker.com':
    server => 'hkps.pool.sks-keyservers.net',
    source => 'https://download.docker.com/linux/debian/gpg',
    id     => '9DC858229FC7DD38854AE2D88D81803C0EBFCD88',
  }

  apt::source { 'docker-ce':
    location => 'https://download.docker.com/linux/debian',
    repos    => 'stable',
    release  => 'buster',
  }

}