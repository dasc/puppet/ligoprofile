class ligoprofile::apt::gitlab {
  apt::key  { 'gitlab.com':
    server => 'hkps.pool.sks-keyservers.net',
    source => 'https://packages.gitlab.com/runner/gitlab-runner/gpgkey',
    id     => '1A4C919DB987D435939638B914219A96E15E78F4',
  }

  apt::source { 'gitlab-runner':
    location => 'https://packages.gitlab.com/runner/gitlab-runner/debian/',
    repos    => 'main',
    release  => 'buster',
  }

}
