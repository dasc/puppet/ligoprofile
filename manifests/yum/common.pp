class ligoprofile::yum::common (
    Boolean $cron_enable,
    String $cron_ensure,
    Boolean $cron_hourly_download_updates,
    Boolean $cron_hourly_apply_updates,
    Integer $cron_hourly_sleep,
    String $cron_hourly_exclude,
    String $cron_hourly_debuglevel,
    Boolean $cron_daily_download_updates,
    Boolean $cron_daily_apply_updates,
    Integer $cron_daily_sleep,
    String $cron_daily_exclude,
    String $cron_daily_debuglevel,
    String $cron_email_from,
    String $cron_email_to,
    String $cron_email_host,
    Optional[Hash] $config_options = undef,
    Optional[String] $cleanindex = '0',
) {
    $localfilestore = lookup('ligoprofile::localfilestore')

    Yumrepo <| |> -> Package <| provider != 'rpm' |>
    Yum::Gpgkey <| |> -> Package <| provider != 'rpm' |>

    Yumrepo <| |> ~>
    exec {'/usr/bin/yum clean metadata':
        command     => '/usr/bin/yum --enablerepo \* clean metadata',
        refreshonly => true,
    } -> Package <| provider != 'rpm' |>

    file {'/tmp/yumcleanup.txt':
        ensure  => present,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => $cleanindex,
        notify  => Exec['/usr/bin/yum clean metadata'],
    }

    service {'yum-cron':
        enable => $cron_enable,
        ensure => $cron_ensure,
    }

    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] < '8' {
        package {'yum-cron': ensure => installed, }
        file {'/etc/yum/yum-cron.conf':
            ensure  => present,
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('ligoprofile/yum-cron.conf.epp', {
                'cron_download_updates' => $cron_daily_download_updates,
                'cron_apply_updates'    => $cron_daily_apply_updates,
                'cron_sleep'            => $cron_daily_sleep,
                'cron_exclude'          => $cron_daily_exclude,
                'cron_debuglevel'       => $cron_daily_debuglevel,
                'cron_email_from'       => $cron_email_from,
                'cron_email_to'         => $cron_email_to,
                'cron_email_host'       => $cron_email_host,
            }),
        }

        file {'/etc/yum/yum-cron-hourly.conf':
            ensure  => present,
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('ligoprofile/yum-cron.conf.epp', {
                'cron_download_updates' => $cron_hourly_download_updates,
                'cron_apply_updates'    => $cron_hourly_apply_updates,
                'cron_sleep'            => $cron_hourly_sleep,
                'cron_exclude'          => $cron_hourly_exclude,
                'cron_debuglevel'       => $cron_hourly_debuglevel,
                'cron_email_from'       => $cron_email_from,
                'cron_email_to'         => $cron_email_to,
                'cron_email_host'       => $cron_email_host,
            }),
        }
    }

    file { '/etc/cron.d/yum-metadata':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => "0 6 * * 2 root /usr/bin/yum -y -q clean metadata\n",
    }
    file {['/etc/yum.repos.d/sl7.repo',
            '/etc/yum.repos.d/sl7-testing.repo',
            '/etc/yum.repos.d/sl7-other.repo',
            '/etc/yum.repos.d/sl7-rolling.repo',
            '/etc/yum.repos.d/sl7-security.repo',
            '/etc/yum.repos.d/sl7-fastbugs.repo', ]: ensure => 'absent', } ->
    Class['yum']

    if $facts['os']['name'] == 'Rocky' and $facts['os']['release']['major'] == '8' {
        file {['/etc/yum.repos.d/CentOS-Linux-AppStream.repo',
            '/etc/yum.repos.d/CentOS-Linux-BaseOS.repo',
            '/etc/yum.repos.d/CentOS-Linux-ContinuousRelease.repo',
            '/etc/yum.repos.d/CentOS-Linux-Debuginfo.repo',
            '/etc/yum.repos.d/CentOS-Linux-Devel.repo',
            '/etc/yum.repos.d/CentOS-Linux-Extras.repo',
            '/etc/yum.repos.d/CentOS-Linux-FastTrack.repo',
            '/etc/yum.repos.d/CentOS-Linux-HighAvailability.repo',
            '/etc/yum.repos.d/CentOS-Linux-Media.repo',
            '/etc/yum.repos.d/CentOS-Linux-Plus.repo',
            '/etc/yum.repos.d/CentOS-Linux-PowerTools.repo',
            '/etc/yum.repos.d/CentOS-Linux-Sources.repo',
            '/etc/yum.repos.d/CentOS-AppStream.repo',
            '/etc/yum.repos.d/CentOS-BaseOS.repo',
            '/etc/yum.repos.d/CentOS-ContinuousRelease.repo',
            '/etc/yum.repos.d/CentOS-Debuginfo.repo',
            '/etc/yum.repos.d/CentOS-Devel.repo',
            '/etc/yum.repos.d/CentOS-Extras.repo',
            '/etc/yum.repos.d/CentOS-FastTrack.repo',
            '/etc/yum.repos.d/CentOS-HighAvailability.repo',
            '/etc/yum.repos.d/CentOS-Media.repo',
            '/etc/yum.repos.d/CentOS-Plus.repo',
            '/etc/yum.repos.d/CentOS-PowerTools.repo',
            '/etc/yum.repos.d/CentOS-Sources.repo',
            '/etc/yum.repos.d/AppStream.repo',
            '/etc/yum.repos.d/BaseOS.repo',
            '/etc/yum.repos.d/ContinuousRelease.repo',
            '/etc/yum.repos.d/Debuginfo.repo',
            '/etc/yum.repos.d/Devel.repo',
            '/etc/yum.repos.d/Extras.repo',
            '/etc/yum.repos.d/FastTrack.repo',
            '/etc/yum.repos.d/fasttrack.repo',
            '/etc/yum.repos.d/HighAvailability.repo',
            '/etc/yum.repos.d/Media.repo',
            '/etc/yum.repos.d/centosplus.repo',
            '/etc/yum.repos.d/Plus.repo',
            '/etc/yum.repos.d/PowerTools.repo',
            '/etc/yum.repos.d/Sources.repo',
            '/etc/yum.repos.d/Rocky-Base.repo',
            '/etc/yum.repos.d/osg36-contrib.repo',
            '/etc/yum.repos.d/osg36-development.repo',
            '/etc/yum.repos.d/osg36-empty.repo',
            '/etc/yum.repos.d/osg36-testing.repo',
            '/etc/yum.repos.d/osg36.repo',
            '/etc/yum.repos.d/osg-contrib-debuginfo.repo',
            '/etc/yum.repos.d/osg-debuginfo.repo',
            '/etc/yum.repos.d/osg-development-debuginfo.repo',
            '/etc/yum.repos.d/osg-testing-debuginfo.repo',
            '/etc/yum.repos.d/osg-upcoming-debuginfo.repo',
            '/etc/yum.repos.d/osg-upcoming-development-debuginfo.repo',
            '/etc/yum.repos.d/osg-upcoming-development.repo',
            '/etc/yum.repos.d/osg-upcoming.repo',
            '/etc/yum.repos.d/osg-upcoming-testing-debuginfo.repo',
            '/etc/yum.repos.d/osg-upcoming-testing.repo',
            ]: ensure => 'absent', } ->
    Class['yum']
    }
    if $facts['os']['name'] == 'Rocky' and $facts['os']['release']['major'] == '9' {
        file {['/etc/yum.repos.d/ldas-restricted.repo',
            '/etc/yum.repos.d/condor.repo',
            '/etc/yum.repos.d/machine-learning.repo',
            '/etc/yum.repos.d/zfs-testing.repo',
            '/etc/yum.repos.d/cuda.repo', ]: ensure => 'absent', }
        -> Class['yum']
    }

    file {['/etc/yum.repos.d/lscsoft-production-debuginfo.repo',
           '/etc/yum.repos.d/lscsoft-testing-debuginfo.repo']:
        ensure => 'absent',
    }

    class {'::yum':
        config_options => $config_options,
        repos          => lookup('ligoprofile::yum::repos'),
        gpgkeys        => lookup('ligoprofile::yum::gpgkeys'),
        managed_repos  => lookup('ligoprofile::yum::managed_repos'),
    }

    # This repo should go away soon.
    file {'/etc/yum.repos.d/htcondor-private.repo':
        ensure => 'absent',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => "${localfilestore}/etc/yum.repos.d/htcondor-private.repo",
        notify => Exec['/usr/bin/yum clean metadata'],
    } -> Package <| |>

    lookup('yum::versionlock', {default_value => {}} ).each | String $name, Optional[Hash] $versionlockinfo | {
        yum::versionlock{"${name}":
            * => $versionlockinfo,
        }
    }

    file {'/etc/grid-security/osg-ca-bundle.crt':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        seltype => 'cert_t',
        source  => 'puppet:///modules/ligoprofile/etc/grid-security/osg-ca-bundle.crt',
    }

    file {'/etc/pki/tls/certs/osg-ca-bundle.crt':
        ensure => 'link',
        target => '/etc/grid-security/osg-ca-bundle.crt'
    }

    file {'/etc/grid-security/all-ca-bundle.crt':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        seltype => 'cert_t',
        source  => 'puppet:///modules/ligoprofile/etc/grid-security/all-ca-bundle.crt',
    }

    file {'/etc/pki/tls/certs/all-ca-bundle.crt':
        ensure => 'link',
        target => '/etc/grid-security/all-ca-bundle.crt'
    }
}
