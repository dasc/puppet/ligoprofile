class ligoprofile::cvmfs::client (
) {
    $ligo_data_frames=lookup('ligoprofile::cvmfs::ligo_data_frames')

    $install_options = $facts['os']['release']['major'] ? {
        '7' => [{'--enablerepo' => 'osg'}, {'--enablerepo' => 'sl-fastbugs'},],
        default => [{'--enablerepo' => 'osg'}],
    }

    package {'osg-oasis':
        ensure => lookup('ligoprofile::cvmfs::oasis_version'),
	install_options => $install_options,
	#require => Yumrepo['osg'],
    }
    package {'cvmfs':
        ensure => lookup('ligoprofile::cvmfs::cvmfs_version'),
	install_options => $install_options,
	#require => Yumrepo['osg'],
    }
    package { 'cvmfs-config-osg':
        ensure => lookup('ligoprofile::cvmfs::config_osg_version'),
	install_options => {'--enablerepo' => 'osg'},
	#require => Yumrepo['osg'],
    }
    autofs::addmount{'cvmfs':
        mounts => [{
	    mountpoint => '/cvmfs',
	    map => '/etc/auto.cvmfs',
	}],
	require => Package['cvmfs'],
    }
    if $ligo_data_frames {
        file {'/etc/cvmfs/config.d/ligo.osgstorage.org.local':
            ensure => 'file',
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => "LIGO_DATA_FRAMES=/ceph/frames\n",
            require => Package['cvmfs'],
            notify => Exec['wipe_cvmfs_cache'],
        }
        file {'/etc/cvmfs/config.d/oasis.opensciencegrid.org.local':
            ensure => 'file',
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => "LIGO_DATA_FRAMES=/ceph/frames\n",
            require => Package['cvmfs'],
            notify => Exec['wipe_cvmfs_cache'],
        }
    }
    file { '/etc/cvmfs/default.local':
	ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('ligoprofile/etc/cvmfs/default.local.epp', {
	    squid_proxy => lookup('ligoprofile::squid::cluster_proxy'),
            squid_failover => lookup('ligoprofile::squid::cluster_failover_proxy'),
            quota_gb => 5000,
        }),
	require => Package['cvmfs'],
        notify => Exec['wipe_cvmfs_cache'],
    }

    exec { 'wipe_cvmfs_cache':
       command     => '/usr/bin/cvmfs_config wipecache',
       refreshonly => true,
    }
}
