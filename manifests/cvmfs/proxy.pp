class ligoprofile::cvmfs::proxy (
) {

      include stdlib

      class { 'squid':
          cache_replacement_policy => 'heap LFUDA',
          coredump_dir => '/var/spool/squid',
      }

      squid::acl { 'manager':
        type => 'proto cache_object',
      }
      squid::acl { 'localnet':
        type    => src,
        entries => ['10.0.0.0/8'],
      }
      squid::acl { 'to_localnet':
        type => dst,
        entries => ['10.0.0.0/8'],
      }

      squid::acl { 'SSL_ports':
        type    => port,
        entries => ['443'],
      }
      squid::acl { 'Safe_ports':
        type    => port,
        entries => ['80', '21', '443', '70', '210', '1025-65535', '280', '488', '591', '777'],
      }
      squid::acl { 'CONNECT':
        type => method,
        entries => ['CONNECT'],
      }

      squid::http_access{ 'manager localhost':
        action => allow,
      }
      squid::http_access{ 'manager':
        action => deny,
      }
      squid::http_access{ '!Safe_ports':
        action => deny,
      }
      squid::http_access { 'localnet':
        action => allow,
      }
      squid::http_access { 'localhost':
        action => allow,
      }
      squid::http_access{ 'all':
        action => deny,
      }
      
      squid::http_port{ '3128':
        port => 3128,
      }


      squid::cache_dir { '/var/spool/squid':
        type => 'ufs',
        options => '20480 16 256',
      }
      squid::cache { 'to_localnet' :
        action => deny,
      }

      squid::refresh_pattern { '.' :
        min => 0,
        max => 4320,
        percent => 20,
      }

      squid::refresh_pattern { '^ftp' :
        min => 1440,
        max => 10080,
        percent => 20,
      }
      squid::refresh_pattern { '^gopher' :
        min => 1440,
        max => 0,
        percent => 0,
      }
      squid::refresh_pattern { '(/cgi-bin/|\?)' :
        case_sensitive => false,
        min => 0,
        max => 0,
        percent => 0,
        order => '61',
      }
}
