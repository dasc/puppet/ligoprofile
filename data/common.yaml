---
lookup_options:
    ligoprofile::mlocate::prunepaths:
        merge: 'unique'
    ligoprofile::mlocate::prunefs:
        merge: 'unique'
    ligoprofile::mlocate::prunenames:
        merge: 'unique'
    ligoprofile::software::common::packages:
        merge: 'deep'
    ligoprofile::software::dmt::packages:
        merge: 'deep'
    ligoprofile::software::user::packages:
        merge: 'deep'
    ligoprofile::yum::common::config_options:
        merge:
            strategy: 'deep'
            knockout_prefix: '--'
            merge_hash_arrays: true
    ligoprofile::yum::repos:
        merge:
            strategy: 'deep'
            knockout_prefix: '--'
            merge_hash_arrays: true
    ligoprofile::yum::gpgkeys:
        merge:
            strategy: 'deep'
            merge_hash_arrays: true
    ligoprofile::yum::managed_repos:
        merge: 'unique'
    ligoprofile::yum::os_default_repos:
        merge: 'unique'
    ligoprofile::yum::repo_exclusions:
        merge: 'unique'

ligoprofile::puppetmaster: 'puppet'
ligoprofile::localfilestore: 'puppet://%{lookup("ligoprofile::puppetmaster")}/modules/ligoprofile'
ligoprofile::condorfilestore: 'puppet://%{lookup("ligoprofile::puppetmaster")}/modules/ligoprofile'
ligoprofile::untrackedfilestore: 'puppet://%{lookup("ligoprofile::puppetmaster")}/ldas'

ligoprofile::rsync::base::package_ensure: 'installed'
ligoprofile::rsync::server::use_xinetd: false
ligoprofile::iccversion: '2020u4'
ligoprofile::software::node_exporter::args: '--no-collector.filesystem --collector.textfile.directory=/etc/node_exporter/txtfiles.d'
ligoprofile::cvmfs::oasis_version: '20-7.osg36.el7'
ligoprofile::cvmfs::cvmfs_version: '2.11.2-1.osg36.el7'
ligoprofile::cvmfs::config_osg_version: '2.5-2.osg36.el7'
ligoprofile::cvmfs::ligo_data_frames: '/ceph/frames'

ligoprofile::yum::common::cron_hourly_download_updates: false
ligoprofile::yum::common::cron_hourly_apply_updates: false
ligoprofile::yum::common::cron_hourly_sleep: 15
ligoprofile::yum::common::cron_hourly_exclude: ''
ligoprofile::yum::common::cron_hourly_debuglevel: '-4'
ligoprofile::yum::common::cron_daily_download_updates: false
ligoprofile::yum::common::cron_daily_apply_updates: false
ligoprofile::yum::common::cron_daily_sleep: 360
ligoprofile::yum::common::cron_daily_exclude: 'kernel* openafs* *-kmdl-* kmod-* *firmware*'
ligoprofile::yum::common::cron_daily_debuglevel: '-2'
ligoprofile::yum::common::cron_enable: false
ligoprofile::yum::common::cron_ensure: 'stopped'
ligoprofile::yum::common::cron_email_from: 'root@localhost'
ligoprofile::yum::common::cron_email_host: 'localhost'
ligoprofile::yum::common::cron_email_to: 'root'
# Increase this number to trigger a yum metadata purge
ligoprofile::yum::common::cleanindex: '2023050901'

ligoprofile::ldap::uri: ['ldap://ldap']
ligoprofile::ldap::base: 'dc=ligo,dc=org'
ligoprofile::ldap::user::base: ''
ligoprofile::ldap::kagra: 'o=KAGRA-LIGO,o=CO,dc=gwastronomy-data,dc=cgca,dc=uwm,dc=edu'
ligoprofile::ldap::tlsreqcert: 'never'
ligoprofile::ldap::tlscacert: '/etc/openldap/certs/ca-bundle.crt'
ligoprofile::ldap::defaultshell: '/sbin/nologin'
ligoprofile::sssd::domains: ['default', 'kagra']

ligoprofile::kerberos::realm: 'LIGO.ORG'
ligoprofile::kerberos::kdcs: ['auth5.ligo.org:88', 'auth1.ligo.org:88', 'auth2.ligo.org:88', 'auth3.ligo.org:88', 'auth4.ligo.org:88']
ligoprofile::kerberos::domains:
  - realm: 'LIGO.ORG'
    domain_list:
    - 'lsckerberos1.gravity.psu.edu'
    - 'lsckerberos2.phys.psu.edu'
    - 'lsc3.ligo.org'
    - '.ligo.org'
    - 'ligo.org'
ligoprofile::kerberos::default_domain: 'ligo.org'
ligoprofile::kerberos::admin_server: 'auth5.ligo.org:749'

ligoprofile::apt::repos:
  pve-no-subscription:
    descr: "No subscription repo of proxmox"
    url: "deb http://download.proxmox.com/debian/pve buster pve-no-subscription"
    gpgkey:  http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg

ligoprofile::mlocate::prunefs:
  - '9p'
  - 'afs'
  - 'anon_inodefs'
  - 'auto'
  - 'autofs'
  - 'bdev'
  - 'binfmt_misc'
  - 'cgroup'
  - 'cifs'
  - 'coda'
  - 'configfs'
  - 'cpuset'
  - 'debugfs'
  - 'devpts'
  - 'ecryptfs'
  - 'exofs'
  - 'fuse'
  - 'fuse.fuse_dfs'
  - 'fuse.sshfs'
  - 'fusectl'
  - 'gfs'
  - 'gfs2'
  - 'gpfshugetlbfs'
  - 'inotifyfs'
  - 'iso9660'
  - 'jffs2'
  - 'lustre'
  - 'mqueue'
  - 'ncpfs'
  - 'nfs'
  - 'nfs4'
  - 'nfsd'
  - 'pipefs'
  - 'procramfs'
  - 'rootfs'
  - 'rpc_pipefs'
  - 'samfs'
  - 'securityfs'
  - 'selinuxfs'
  - 'sfs'
  - 'sockfs'
  - 'sysfs'
  - 'tmpfs'
  - 'ubifs'
  - 'udf'
  - 'usbfs'
  - 'ceph'
  - 'fuse.ceph'
  - 'fuse.glusterfs'

ligoprofile::mlocate::prunenames:
  - '.git'
  - '.hg'
  - '.svn'
  - '.bzr'
  - '.arch-ids'
  - '{arch}'
  - 'CVS'

ligoprofile::mlocate::prunepaths:
  - '/afs'
  - '/media'
  - '/mnt'
  - '/net'
  - '/sfs'
  - '/tmp'
  - '/udev'
  - '/var/cache/ccache'
  - '/var/lib/yum/yumdb'
  - '/var/lib/dnf/yumdb'
  - '/var/spool/cups'
  - '/var/spool/squid'
  - '/var/tmp'
  - '/var/lib/ceph'
