# scitoken_kid.rb

Facter.add('kid') do
    setcode do 
      Facter::Core::Execution.execute('/usr/bin/jq -r .keys[0].kid /etc/condor/scitokens.d/public-key.jwks')
    end
end
