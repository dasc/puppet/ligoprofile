Facter.add(:preferredsubnet) do
    setcode do
        preference = {'10.13.0.0' => 1,
                      '10.9.0.0' => 2,
                      '10.8.0.0' => 3,
                      '10.6.0.0' => 4,
                      '208.69.128.64' => 5,
        }

        case Facter.value(:networking)['network']
        when '10.13.0.0'
            '10.13.0.0'
        when '10.9.0.0'
            '10.9.0.0'
        when '10.8.0.0'
            '10.8.0.0'
        when '10.6.0.0'
            '10.6.0.0'
        when '208.69.128.64'
            result = '208.69.128.64'
            Facter.value(:networking)['interfaces'].each do |interface, values|
                if preference[values['network']]
                    if preference[values['network']] < preference[result]
                        result = values['network']
                    end
                end
            end
            result
        else
            Facter.value(:networking)['network']
        end
    end
end
