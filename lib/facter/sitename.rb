Facter.add(:sitename) do
    setcode do
        sitename = Facter::Core::Execution.execute("cat /etc/sitename 2>/dev/null")
        sitename == '' ? 'undefined' : sitename
    end
end
