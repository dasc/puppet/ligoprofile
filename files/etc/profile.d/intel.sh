#
# LIGO-specific setup for Intel oneAPI compiler suite
#
# ~/.nointel           implies no icc or environment setup
# ~/.oneapi_config.txt load custom Intel oneAPI modules and versions
# 

if [[ $USER == "root" ]] ; then
  return
fi

# Do not process compiler selection for non-interactive session
if [[ $TERM == "dumb" ]]; then
  return
fi

user_message() {
  if [ $# -gt 1 ] ; then
    if [ -t 0 ]; then
      echo $@
    fi
  fi
}


#
# Glob for user opt-in and opt-out files in their home directory
#
#if [[ -n "$ZSH_VERSION" ]]; then
#  setopt nullglob
#elif [[ -n "$KSH_VERSION" ]]; then
#  # there is no toggle to set nullglob
#  :
#else
#  shopt -s nullglob
#fi

#if [[ -n "$ZSH_VERSION" ]]; then
#  :
#elif [[ -n "$KSH_VERSION" ]]; then
  # there is no toggle to set nullglob so check for "*"
#  star="\*"
#else
#  shopt -u nullglob
#fi


#
# Process opt-in/opt-out requests
#
if [ -f $HOME/.nointel ]; then
  #
  # Request no Intel setup whatsoever
  #
  user_message "ICC: opt-out of setup requested."
  return 0
fi


if [ ! -f /opt/intel/oneapi/setvars.sh ]; then
  user_message "ICC: not currently available."
  return 0
fi

OUT=$(mktemp)

if [ -f $HOME/.oneapi_config.txt ]; then
    #
    # Load custom Intel oneAPI modules
    #
    configfile=$HOME/.oneapi_config.txt
    message="ICC: Custom Intel oneAPI modules active"
else
    #
    # Load latest Intel oneAPI modules (except Python & MPI) 
    #
    configfile=/opt/intel/oneapi/oneapi_config.txt
    message=""
fi


source /opt/intel/oneapi/setvars.sh --config=$configfile > $OUT 2>&1 && grep initialized $OUT | tr '\n' ' ' && icx --version | head -1
user_message "$message"

/bin/rm -f $OUT

return 0
