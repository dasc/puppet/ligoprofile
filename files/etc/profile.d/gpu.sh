if [[ $USER == "root" ]] ; then
  return
fi

allgpudevs=()
mygpudevs=()
freegpudevs=()
for gpudev in /dev/nvidia[0-9]* ; do
    mode=$(stat -c %a ${gpudev})
    devindex=$(echo ${gpudev} | sed -e 's/.*nvidia//' )
    if [ ${mode} == "660" ] ; then
	if [ -w ${gpudev} ] ; then
	    mygpudevs+=( ${devindex} )
	fi
    else
        freegpudevs+=( ${devindex} )
    fi
    allgpudevs+=( ${devindex} )
done

if [ ${#mygpudevs[@]} -gt 0 ] ; then
    CUDA_VISIBLE_DEVICES=$(IFS=, ; echo "${mygpudevs[*]}")
else
    CUDA_VISIBLE_DEVICES=$(IFS=, ; echo "${freegpudevs[*]}")
fi

TF_FORCE_GPU_ALLOW_GROWTH=true

export CUDA_VISIBLE_DEVICES
export TF_FORCE_GPU_ALLOW_GROWTH

