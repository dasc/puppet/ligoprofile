#
# Disable throttling GPU memory
#
# 
export XLA_PYTHON_CLIENT_PREALLOCATE=false 
export JAX_COMPILATION_CACHE_DIR=/local/${USER}/jax_cache
