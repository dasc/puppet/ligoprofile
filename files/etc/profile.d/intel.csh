#
# LIGO-specific setup for Intel oneAPI compiler suite
#
# ~/.nointel           implies no icc or environment setup
# ~/.oneapi_config.txt load custom Intel oneAPI modules and versions
# 

if ( $USER == "root" ) then
  exit 0
endif

# Do not process compiler selection for non-interactive session
if ( $TERM == "dumb" ) then
  exit 0
endif

#
# only print messages when in a login shell, i.e., $0 is "-csh" or "-tcsh"
#
alias user_message 'if ( X$0:s/tcsh//:s/csh// == "X-" ) echo \!*'

#
# Process opt-in/opt-out requests
#
if ( -f $HOME/.nointel ) then
  #
  # Request no Intel setup whatsoever
  #
  user_message "ICC: opt-out of setup requested."
else
  #
  # C-shell is not currently supported by oneAPI
  #
  user_message "ICC: Intel oneAPI compiler environment not supported in csh."
  user_message "ICC: See /opt/intel/oneapi/csh_workaround.txt for more info."
endif
